package ua.com.epam.businessobject;

import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.pageobject.SentMessagePage;

@Log4j2
public class SentMessageBO {
    private final SentMessagePage sentMessagePage;

    public SentMessageBO() {
        this.sentMessagePage = new SentMessagePage();
    }

    @Step("Getting message text.")
    public String getMessageText() {
        log.info("Getting message text.");

        return sentMessagePage.getSentMessage()
                .getTextAfterStalenessOfElement(sentMessagePage.getFirstMessage());
    }

    @Step("Selecting all letters and delete.")
    public SentMessageBO deleteFirstLetter() {
        log.info("Selecting all letter.");
        sentMessagePage.getAllCheckbox().clickIfNotSelected();

        log.info("Delete a letter.");
        sentMessagePage.getDeleteMsg().clickAfterElementToBeClickable();
        sentMessagePage.getRefSendInClearAreaMsg().getTextAfterDocumentReadyState();
        return this;
    }
}
