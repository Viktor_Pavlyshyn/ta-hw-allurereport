package ua.com.epam.businessobject;

import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.pageobject.GmailHomePage;

@Log4j2
public class GmailHomeBO {

    private final GmailHomePage gmailHomePage;

    public GmailHomeBO() {
        this.gmailHomePage = new GmailHomePage();
    }

    @Step("Setting recipient - '{0}', topic - '{1}', message - '{2}' and sending letter.")
    public GmailHomeBO writeAndSendMessage(String recipient, String topic, String textMessage) {
        log.info("Click by button 'Write'.");
        gmailHomePage.getComposeButton().clickAfterDocumentReadyState();

        log.info("Setting recipient - {}.", recipient);
        gmailHomePage.getRecipientArea()
                .setTextAfterPresenceOfAllElementsLocatedBy(recipient, gmailHomePage.getRecipientAreaXpath());

        log.info("Setting topic - {}.", topic);
        gmailHomePage.getTopicArea().cleanBeforeSendKeys(topic);

        log.info("Setting message - {}.", textMessage);
        gmailHomePage.getMessageArea().cleanBeforeSendKeys(textMessage);

        log.info("Sending letter.");
        gmailHomePage.getSendButton().clickAfterElementToBeClickable();

        return this;
    }

    @Step("Navigate to 'sent letter' tab.")
    public SentMessageBO navigateToSentLetter() {
        log.info("Navigate to 'sent letter' tab.");
        gmailHomePage.getSentMessageTab()
                .clickAfterStalenessOfElement(gmailHomePage.getSentMsgTabXpath());

        return new SentMessageBO();
    }
}
