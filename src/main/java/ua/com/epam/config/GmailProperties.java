package ua.com.epam.config;

public interface GmailProperties {
    String GMAIL_LOGIN_PAGE = "gmail";
    String RECIPIENT = "google.recipient";
    String TOPIC = "google.topic";
}
